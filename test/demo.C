{
    {
        TFile f("testData.root","RECREATE");
        TTree t("testTree","testTree"); TTree t2("tree2","tree2");
        double x = 0; t.Branch("x",&x);t2.Branch("x",&x);
        for(int i=20;i>0;i-=2) {x=i;t.Fill();x+=1; t2.Fill();}
        t.Write();t2.Write();
    }



    xFrame mcChain = TChain("testTree");
    mcChain->SetName("mcChain");
    mcChain->get<TChain>()->Add("testData.root");

    mcChain |= "myAna"; // extends frame horizontally with a new column
    mcChain["myAna"]->SetTitle("my analysis");
    mcChain["myAna"]->setStringAttribute("filename","myAna.C");
    mcChain |= {"myFunc","x*3"};
    mcChain.Scan({"x","myAna","myFunc"});

    xFrame dataChain = TChain("tree2"); dataChain->SetName("dataChain");
    dataChain->get<TChain>()->Add("testData.root");
    dataChain |= "myAna"; // extends frame horizontally with a new column
    dataChain["myAna"]->setStringAttribute("filename","myAna.C");
    allData = mcChain + dataChain;

    // this is designed to test that multiple components can derive from the same common dataframe without copying
    std::cout << "entries = " << (mcChain["x>10"] + mcChain["x<5"]).GetEntries() << std::endl; // should be 7

    mcChain |= {"xx","int(x)"};
    myMap = mcChain["xx"].Map("myMap");
    myMap[20] = 4; myMap[18] = 8;
    mcChain |= myMap;

    mcChain.Scan({"xx","myMap"});

    mcChain *= "2*x"; // adds a "weight" to the frame

    zjets = mcChain["myMap==4"];zjets->SetName("zjets"); zjets->SetTitle("z+jets"); zjets->SetFillColor(kBlue); zjets->SetDrawOption("hist");
    ttbar = mcChain["myMap==8"];ttbar->SetName("ttbar"); ttbar->SetTitle("t#bar{t}"); ttbar->SetFillColor(kRed); ttbar->SetDrawOption("hist");
    _mc = zjets + ttbar;_mc->SetName("mc"); _mc->SetTitle("MC"); _mc->SetDrawOption("E2"); _mc->SetFillStyle(3005); _mc->SetMarkerStyle(0); _mc->SetFillColor(_mc->GetLineColor());
    _data = dataChain["1"];_data->SetName("data");_data->SetTitle("Data"); _data->SetDrawOption("pEX0");

    region1 = xCut("x<5"); region2 = xCut("x>10"); //xCuts define filters on xFrames

    plot = _mc | _data;
    plot["x"]->setBinning(RooUniformBinning(0,30,10));
    plot["x"]->setUnit("GeV");

    plot->setStringAttribute("cacheFileName","file.root");

    // want to be able to do:
    plot["myAna"]->setBinning(RooUniformBinning(0,300,100));
    TCanvas c; c.Divide(2,1); c.cd(1); plot.Draw({"x"},"postpone");
    c.cd(2); plot.Draw({"myAna"},"postpone");
    //xFrame::Process();


//    plot.Draw({"x"});
//    xPlot::BuildLegend({.format="%title% %int% +/- %interr%"});
//    xPlot::AddRatio("mc");


    // can create a map with multiple indices too:
    ee = mcChain + "v1:=int(x)" + "v2:=2*int(x)";
    twoMap = ee[{"v1","v2"}].Map("myTwoMap");
    twoMap[{20,40}] = 7;
    ee += twoMap;
    ee.Scan({"v1","v2","myTwoMap"});


}