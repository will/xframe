
#include <gtest/gtest.h>

#include "xFrame/xFrame.h"

TEST(Test_xFrame, cacheTest) {

    // create test input tree
    {
        TFile f("test.root","RECREATE");
        TTree t("testTree","testTree");
        double a = 1; t.Branch("a",&a);
        int b = 0; t.Branch("b",&b);
        std::vector<float> c = {}; t.Branch("c",&c);

        for(int i=0;i<10;i++) {
            b+=1;
            c.resize(b,b);
            a+=0.5;
            t.Fill();
        }
        t.Write();
    }

    // Create xFrame that points at this data - do it with a TChain
    // you can either create the TChain ahead of time or embed it straight away in the xframe and then
    // interact with it like this:
    xFrame df = TChain("testTree");
    df->get<TChain>()->Add("test.root");
    df->SetNameTitle("myChain","My Chain");

    // "scan"" the first 5 entries of the frame
    df.Scan({"a","c","c*a"}); // df.Scan({cols},"",N) to scan N entries




}