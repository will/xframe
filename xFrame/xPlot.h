#pragma once

#include "TVirtualPad.h"
#include "TString.h"
#include "TLegend.h"
#include "THStack.h"
#include "TPaveText.h"
#include "TCanvas.h"
#include "TGraph.h"

#include <iostream>
#include <functional>

struct xPlotOpts {
    TVirtualPad* pad = nullptr;
    TString format = "%title%";//"%title% %int% +/- %interr%";
    bool build = false;
};

class xPlot {
public:

    // Use this class to attach a function to a TObject that is drawn
    // This is like a TExec except it works with std::functions
    template<typename T> class TFunction : public TNamed {
      public:
        TFunction(const char* name, const T& _func) : TNamed(name,name), fFunc(_func) { }
        void Paint(Option_t* opt="") override { fFunc(); }
        T fFunc; //!
    };

    // maybe rename GetObject?
    template <typename T=TObject> static T* GetPrimitive(const char* name, TObject* parent = nullptr) {

        if (!parent) {
            parent = gPad;
            if (!parent) return nullptr;
            parent = gPad->GetCanvas(); // actually start from the canvas
        }

        if (strlen(name)==0) return dynamic_cast<T*>(parent);

        std::string sName(name);
        std::string sPart = (sName.find('/')!=std::string::npos) ? sName.substr(0,sName.find('/')) : sName;
        sName = (sName==sPart) ? "" : sName.substr(sName.find('/')+1);

        // find sPart and then GetPrimitive on it with sName
        if( auto s = dynamic_cast<THStack*>(parent); s ) {
            auto h = s->GetHists()->FindObject(sPart.c_str());
            if (h) return GetPrimitive<T>(sName.c_str(),h);
            return nullptr;
        } else {
            auto h = parent->FindObject(sPart.c_str());
            if (h) return GetPrimitive<T>(sName.c_str(),h);

            // if was a pad ... treat any stacks as if the hists were primitives too
            if(auto p = dynamic_cast<TVirtualPad*>(parent)) {
                // loop over primitives ... special case of THStack primitive - treat its hists as if they were in the primitive list
                for(auto&& o : *p->GetListOfPrimitives()) {
                    if (auto s = dynamic_cast<THStack*>(o); s) {
                        auto h = s->GetHists()->FindObject(sPart.c_str());
                        if (h) return GetPrimitive<T>(sName.c_str(),h);
                    } else if(auto pp = dynamic_cast<TVirtualPad*>(o); pp) {
                        auto check = GetPrimitive<T>(name,pp); // allow skip through a pad
                        if (check) return check;
                    }
                }
            }

            return nullptr;
        }


        //if (dynamic_cast<TVirtualPad*>(pad) && dynamic_cast<TVir != pad->GetCanvas()) {
        //    return GetPrimitive(name,pad);
        //}

        return nullptr;

    }

    static void Print(TObject* parent = nullptr, bool showHidden=false, int indent=0);


    static TLegend* GetLegend(const xPlotOpts& opts = {});


    static bool BuildLegend(const xPlotOpts& opts = {}) {
        xPlotOpts _opts = opts; _opts.build = true;
        return GetLegend(_opts);
    }

    static TPaveText* GetPave(const char* paveName, TVirtualPad* pad = nullptr, bool build=false) {
        if (!pad) pad = gPad;
        if (!pad) return nullptr;

        auto _pave = dynamic_cast<TPaveText*>(pad->GetPrimitive(paveName));
        if (!_pave && build) {
            _pave = new TPaveText(pad->GetLeftMargin(), 1. - pad->GetTopMargin()-0.02, 1. - pad->GetRightMargin(), 1.,
                                  "NBNDC");
            _pave->SetTextAlign(33);
            _pave->SetBit(kCanDelete);
            _pave->SetName(paveName);
            _pave->SetTextSize(12);_pave->SetFillStyle(0);_pave->SetLineWidth(0);_pave->SetMargin(0);
            _pave->Draw();
        }
        return _pave;
    }
    static TPaveText* GetLabels(TVirtualPad* pad = nullptr, bool build=false) { return GetPave("labels",pad,build); }

    static bool AddLabel(const char* label, TVirtualPad* pad = nullptr) {
        auto l = GetLabels(pad,true); if (!l) return false;
        l->AddText(label); return true;
    }

    static bool AddRatio(const char* denominator, double height=0.3, TVirtualPad* pad = nullptr);

    static TVirtualPad* AddAuxPad(const char* name, const char* title, double height=0.3, TVirtualPad* pad = nullptr);


    static void Update(TVirtualPad* pad=nullptr);

};
