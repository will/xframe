#pragma once

#include "TVirtualPad.h"
#include "TExec.h"
#include "THStack.h"
#include "ROOT/RDFHelpers.hxx"

#include "xFrame/xVar.h"
#include "xFrame/xCut.h"
#include "xFrame/xMap.h"

class xFrame {
public:

    class HiddenObject : public TNamed {
    public:
        // only purpose is to allow adding an object to a list of functions without it getting drawn
        HiddenObject() : TNamed() { }
        HiddenObject(const char* name, const char* title) : TNamed(name,title) { }
        TObject* fObj = nullptr;
        ClassDef(HiddenObject,0)
    };

    static void Book(ROOT::RDF::RResultHandle result, const std::shared_ptr<xVar::NamedNode>& rootNode = nullptr, const std::function<void(ROOT::RDF::RResultHandle&)>& finalize = [](const ROOT::RDF::RResultHandle&){});
    static std::vector<std::tuple<ROOT::RDF::RResultHandle, std::function<void(ROOT::RDF::RResultHandle&)>, std::shared_ptr<xVar::NamedNode>>> fBookings;
    static void Process(int progress=100000);

    xFrame() { }
    xFrame(const TObject& obj);
    xFrame(const TChain& c); // specialization for TChain because cloning chain is meaningless

    xFrame(const char* name, const char* title="") : xFrame(RooRealVar(std::string(name).find(":=")!=std::string::npos && strlen(title)==0 ? std::string(name).substr(0,std::string(name).find(":=")).c_str() : name,
                                                                         std::string(name).find(":=")!=std::string::npos && strlen(title)==0 ? std::string(name).substr(std::string(name).find(":=")+2).c_str() : title,1)) {
        // copy formula to attribute in case use overrides title
        if(std::string(name).find(":=")!=std::string::npos && strlen(fVar->GetTitle())) fVar->setStringAttribute("formula",fVar->GetTitle());
    }

    // should these return a copy of self instead of modifying self? - mimic RDataFrame probably better
    // to do that, need to move modifiers into xFrame (if in fVars will get shared by the original)

    // This isn't working :(
//    template<typename... T> xFrame Define(std::string_view name, T&& ... t) {
//        // when C++20 available, should replace this with a perfect forward
//        xFrame newColumn(name.data(),name.data()); newColumn->fModifiers.emplace_back(
//                [name,args = std::make_tuple(std::forward<T>(t) ...)](ROOT::RDF::RNode node)mutable{
//                    return std::apply([name,&node](auto&& ... args){
//                        node.Define(name,std::forward<T>(args)...);
//                    }, std::move(args));});
//        return *this + newColumn;
//    }
    template<typename T> static xFrame Define(std::string name, T expression, const ROOT::RDFDetail::ColumnNames_t& columns) {
        // when C++20 available, should replace this with a perfect forward
        xFrame newColumn(name.data(),name.data()); newColumn->fModifiers.emplace_back( [name,expression,columns](ROOT::RDF::RNode node) {
            try {
                return node.Define(name,expression,columns);
            } catch(...) {
                Warning("Define","Failed to define %s",name.data());
                return node;
            }
        } );
        return newColumn;
    }
    static xFrame Define(std::string_view name, std::string_view expression) {
        // when C++20 available, should replace this with a perfect forward
        xFrame newColumn(name.data(),name.data()); newColumn->fModifiers.emplace_back( [=](ROOT::RDF::RNode node) { return node.Define(name,expression);} );
        return newColumn;
    }

    xVar* operator->() const { return fVar.get(); }


    xFrame& operator+=(const xFrame& rhs) {
//        if (TString(rhs->GetName()).EndsWith(".root")) {
//            TString s(fVar->getStringAttribute("datasource"));
//            fVar->setStringAttribute("datasource",s + (s=="" ? "" : ";") + rhs->GetName());
//            return *this;
//        }

        fFrames.push_back(rhs);
        fFrames.back().fType = PLUS;
        fVar->addServer(*fFrames.back().fVar.get());
        /*if (fFrames.back()->isFundamental()) {
            // add the fundamental as the server
            fVar->addServer(*fFrames.back()->fObj.get());
        } else {
            fVar->addServer(*fFrames.back().fVar.get());
        }*/
        return *this;
    }

    xFrame& operator|=(const xFrame& s) {
        // extending horizontally
        auto& out = operator+=(s);
        out.fFrames.back().fType = OR;
        return out;
    }

    xFrame operator+(const xFrame& rhs) & {
        xFrame out(TString::Format("%s+%s",fVar->GetName(),rhs->GetName()),TString::Format("%s+%s",strlen(fVar->GetTitle()) ? fVar->GetTitle() : fVar->GetName(),rhs->GetTitle()));
        out += *this; out += rhs;
        out.fPostDrawAction = fPostDrawAction; // inherit action too
        return out;
    }

    xFrame operator+(const xFrame& rhs) && {
        // if adding to a temporary, just accumulate in the temporary, unless the temporary has cuts (dont want to inherit the cuts)
        if (fCuts.empty() && fVar.use_count()<=1) {
            xFrame out(*this);
            out->SetNameTitle(TString::Format("%s+%s",fVar->GetName(),rhs->GetName()),TString::Format("%s+%s",strlen(fVar->GetTitle()) ? fVar->GetTitle() : fVar->GetName(),rhs->GetTitle()));
            //xFrame out(TString::Format("%s+%s",fVar->GetName(),rhs->GetName()),TString::Format("%s+%s",strlen(fVar->GetTitle()) ? fVar->GetTitle() : fVar->GetName(),rhs->GetTitle()));
            /*out += *this;*/ out += rhs;
            return out;
        } else {
            return (*this) + rhs; // should call method above, I think
        }
    }

    xFrame operator|(const xFrame& rhs) & {
        xFrame out(TString::Format("%s|%s",fVar->GetName(),rhs->GetName()),TString::Format("%s|%s",strlen(fVar->GetTitle()) ? fVar->GetTitle() : fVar->GetName(),rhs->GetTitle()));
        out |= *this; out |= rhs;
        out.fPostDrawAction = fPostDrawAction; // inherit action too
        return out;
    }

    xFrame operator|(const xFrame& rhs) && {
        // if adding to a temporary, just accumulate in the temporary, unless the temporary has cuts (dont want to inherit the cuts)
        if (fCuts.empty() && fVar.use_count()<=1) {
            xFrame out(*this);
            out->SetNameTitle(TString::Format("%s|%s", fVar->GetName(), rhs->GetName()),
                              TString::Format("%s|%s", strlen(fVar->GetTitle()) ? fVar->GetTitle() : fVar->GetName(),
                                              rhs->GetTitle()));
            /*out |= *this;*/ out |= rhs;
            return out;
        } else {
            return (*this) | rhs;
        }
    }

    xFrame operator||(const xFrame& rhs) & {
        xFrame out(TString::Format("%s||%s",fVar->GetName(),rhs->GetName()),TString::Format("%s||%s",strlen(fVar->GetTitle()) ? fVar->GetTitle() : fVar->GetName(),rhs->GetTitle()));
        out |= *this; out.fFrames.back().fType = OROR; out |= rhs; out.fFrames.back().fType = OROR;
        out.fPostDrawAction = fPostDrawAction; // inherit action too
        return out;
    }

    xFrame operator||(const xFrame& rhs) && {
        // if this has nothing but OROR then copy this, otherwise make this a subframe
        bool makeSubframe = false;
        for(auto& f : fFrames) { if(f.fType!=OROR) {
            makeSubframe = true; break;
        } }
        if (makeSubframe) {
            return (*this) || rhs; // will package this as a subframe of the output (i.e. calls method above)
        }
        xFrame out(*this);
        out->SetNameTitle(TString::Format("%s||%s",fVar->GetName(),rhs->GetName()),TString::Format("%s||%s",strlen(fVar->GetTitle()) ? fVar->GetTitle() : fVar->GetName(),rhs->GetTitle()));
        /*out |= *this;*/ out |= rhs; out.fFrames.back().fType = OROR;
        return out;
    }

    xFrame& operator *=(const char* weight) {
        TCut c; if (auto _c = fVar->getStringAttribute("weight")) c *= _c;
        c *= weight;
        fVar->setStringAttribute("weight",c.GetTitle());
        return *this;
    }

    /*xFrame operator+(const xSeries& rhs) {
        return xFrame(*this) += rhs;
    }*/

    xFrame operator[](const std::vector<std::string>& vars) {
        TString vv;
        for(auto& v : vars) {
            if(vv!="") vv+=";";
            vv += v.c_str();
        }
        xFrame out(TString::Format("%s[{%s}]",fVar->GetName(),vv.Data()),TString::Format("%s[{%s}]",strlen(fVar->GetTitle()) ? fVar->GetTitle() : fVar->GetName(),vv.Data()));
        out += *this;
        out->setStringAttribute("selectedColumns",vv);
        return out;
    }

    xFrame operator[](std::initializer_list<std::string> in) {
        return operator[](std::vector<std::string>(in));
    }

    xFrame operator[](const xCut& str) {

        std::function<xFrame*(xFrame*,const std::string&)> findChild;

        findChild = [&findChild](xFrame* f, const std::string& s) {
            if (s=="") return f;
            auto _s = (s.find('.')!=std::string::npos) ? s.substr(0,s.find('.')) : s;
            auto _s2 = (_s==s) ? "" : s.substr(_s.length()+1);
            for(auto& _f : f->fFrames) {
                if(_s == _f->GetName()) {
                    auto r = findChild(&_f,_s2);
                    if(r) return r;
                }
            }
            // allow passthrough of names
            for(auto& _f : f->fFrames) {
                auto r = findChild(&_f,s);
                if(r) return r;
            }

            // if this is a data source frame can also construct a column
            // TODO: Perhaps should allow frames further along to have their own 'column' frames - may lead to less confusion about edits affecting other frames
            if ((*f)->get<TChain>()) {

                // see if defined in any nodes
                for (auto& n: f->getNodes()) {
                    if (n.node.HasColumn(s)) {
                        (*f) |= {s.c_str(),s.c_str()}; // extends horizontally with a new frame (it's really always been there, just make it explicit now).
                        auto out = &(f->fFrames.back());
                        (*out)->setAttribute("isDataColumn"); // so we know this frame isn't an extending frame
                        return out;
                    }
                }
            }
            return (xFrame*)nullptr;
        };

        auto xx = findChild(this,str.GetTitle());
        if (xx) return *xx;
//        for(auto& f : fFrames) {
//            if (strcmp(f->GetName(),str.GetTitle())==0) return f;
//        }
        // create a new xFrame that is a subset of this xFrame
        xFrame out(TString::Format("%s[%s]",fVar->GetName(),str.GetTitle()),TString::Format("%s[%s]",strlen(fVar->GetTitle()) ? fVar->GetTitle() : fVar->GetName(),str.GetTitle()));
        out += *this;
        out.fCuts.push_back(str);
        out.fPostDrawAction = fPostDrawAction; // inherit action too
        // TODO inherit attributes? Probably ... doing stackType for now ...
        if(fVar->getStringAttribute("stackType")) out->setStringAttribute("stackType",fVar->getStringAttribute("stackType"));
        return out;
    }

    Long64_t GetEntries();
    Double_t GetIntegral(); // sum of the weights

//    xFrame& operator()(const std::string& str) {
//        for(auto& s : fFrames) {
//            if (str == s->GetName() && s->isFundamental()) return s;
//        }
//        // try getting from any subframes
//        for(auto& f : fFrames) {
//            try {
//                return f(str);
//            } catch(std::exception&) {
//                // do nothing
//            }
//        }
//        // see if defined in any nodes
//        for(auto& [n,w] : getNodes()) {
//            if(n.HasColumn(str)) {
//                (*this) += RooRealVar(str.c_str(),str.c_str(),0);
//                xFrame& out = this->operator()(str);
//                out->setAttribute("isColumn"); // so we know this frame isn't an extending frame
//                return out;
//            }
//        }
//        throw std::runtime_error("var not found");
//    }


//    xSeries& operator()(const std::string& str) {
//        for(auto& s : fSeries) {
//            if (str == s.GetName()) return s;
//        }
//        // try getting from any subframes
//        for(auto& f : fFrames) {
//            try {
//                return f(str);
//            } catch(std::exception&) {
//                // do nothing
//            }
//        }
//        // see if defined in any nodes
//        for(auto& [n,w] : getNodes()) {
//            if(n.HasColumn(str)) {
//                (*this) += RooRealVar(str.c_str(),str.c_str(),0);
//                return this->operator()(str);
//            }
//        }
//        throw std::runtime_error("var not found");
//    }

    template<typename T=int> xFrame Map(const char* name, T defaultVal=0) const {
        // create a mapping frame with given name that maps all the vars of this frame onto something
        xFrame out;
        if (fVar->getStringAttribute("selectedColumns")) {
            // use those instead
            int nCols = TString(fVar->getStringAttribute("selectedColumns")).CountChar(';')+1;
            auto xm = std::make_shared<xMap<std::vector<int>,T>>(name,"");
            xm->fInput = fVar->getStringAttribute("selectedColumns");
            out.fVar = std::make_shared<xVar>(name,"",1); out.fVar->addServer(*xm);
            out.fVar->UseCurrentStyle();
            out.fVar->fObj = xm;
            return out;
        }
        auto xm = std::make_shared<xMap<int,T>>(name,"",defaultVal);
        xm->fInput = fVar->GetName(); // TODO: support xFrames with multiple columns
        out.fVar = std::make_shared<xVar>(name,"",1); out.fVar->addServer(*xm);
        out.fVar->UseCurrentStyle();
        out.fVar->fObj = xm;
        return out;
    }

    template<typename T> class VarWithArg {
        public:
            VarWithArg(std::shared_ptr<xVar> var, const T& _arg) : arg(_arg), fVar(var) { }
            template<typename W> VarWithArg& operator=(const W& val) {
                if (auto xm = std::dynamic_pointer_cast<xMap<T,W>>(fVar->fObj)) {
                    (*xm)[arg] = val;
                    return *this;
                }
                throw std::runtime_error(TString::Format("%s not a map of correct type",fVar->GetName()).Data());
            }
            const T& arg;
            std::shared_ptr<xVar> fVar;
        };

    VarWithArg<int> operator[](const int& i) {
        return VarWithArg(fVar,i);
    }

    VarWithArg<std::vector<int>> operator[](const std::vector<int>& i) {
        return VarWithArg(fVar,i);
    }

    std::vector<std::string> GetComponentNames() const;
    std::set<std::string> GetColumnNames(bool onlyCommon=true) const;

    std::string GetCacheFileName() const;

    bool getFromCache(TObject* obj, std::string callPath, std::size_t hash);

    void getNodesInternal(std::map<RooAbsArg*,std::vector<xVar::NamedNode>>& out) const;

    std::vector<xVar::NamedNode> getNodes() const {
        // loop through servers, get nodes from xFrame children, add definitions for fundamental children,
        // and add filter for any cut - also each node can have a separate weight.
        std::map<RooAbsArg*,std::vector<xVar::NamedNode>> result;
        getNodesInternal(result);
        return result[fVar.get()];
    };

    bool HasColumn(std::string_view what) const {
        // TODO: fix this for dataframes without data - need to check for servers
        bool out=false;
        for(auto& n : getNodes()) {
            out=true;
            if (!n.node.HasColumn(what)) return false;
        }
        return out;
    }

    TObject* Draw(std::string_view what, int nbins, double low, double high, Option_t* opt="") {
        // check column exists otherwise define ...
        if (!HasColumn(what)) {
            return ((*this) + (std::string("_var0:=")+what.data()).c_str()).Draw("_var0",nbins,low,high,opt);
        }
        operator[](what.data())->SetBins(nbins,low,high);
        return Draw({what.data()},opt);
    }
    TObject* Draw(const std::pair<std::string,std::string>& what, int nbinsx, double lowx, double highx, int nbinsy, double lowy, double highy, Option_t* opt="") {
        // check column exists otherwise define ...
        // check has "what" if not will define column and draw
        for(auto& w : {what.first,what.second}) {
            if (!HasColumn(w)) {
                auto _fr = (*this) + TString::Format("_%zu:=%s", std::hash<std::string>{}(w),
                                                     w.c_str()).Data(); // +what.data()).c_str())
                _fr[TString::Format("_%zu", std::hash<std::string>{}(w)).Data()]->SetTitle(w.c_str());
                auto wCopy = what;
                if (wCopy.first == w) wCopy.first = TString::Format("_%zu", std::hash<std::string>{}(w));
                if (wCopy.second == w) wCopy.second = TString::Format("_%zu", std::hash<std::string>{}(w));
                return _fr.Draw(wCopy,nbinsx,lowx,highx,nbinsy,lowy,highy,opt);
            }
        }
        operator[](what.first.data())->SetBins(nbinsx,lowx,highx);
        operator[](what.second.data())->SetBins(nbinsy,lowy,highy);
        std::vector<std::string> _ww; _ww.push_back(what.first); _ww.push_back(what.second);
        return Draw(_ww,opt);
    }
    TObject* Draw(const std::tuple<std::string,std::string,std::string>& what, int nbinsx, double lowx, double highx, int nbinsy, double lowy, double highy, int nbinsz, double lowz, double highz, Option_t* opt="") {
        // check column exists otherwise define ...
        // check has "what" if not will define column and draw
        for(auto& w : {std::get<0>(what),std::get<1>(what),std::get<2>(what)}) {
            if (!HasColumn(w)) {
                auto _fr = (*this) + TString::Format("_%zu:=%s", std::hash<std::string>{}(w),
                                                     w.c_str()).Data(); // +what.data()).c_str())
                _fr[TString::Format("_%zu", std::hash<std::string>{}(w)).Data()]->SetTitle(w.c_str());
                auto wCopy = what;
                if (std::get<0>(wCopy) == w) std::get<0>(wCopy) = TString::Format("_%zu", std::hash<std::string>{}(w));
                if (std::get<1>(wCopy) == w) std::get<1>(wCopy) = TString::Format("_%zu", std::hash<std::string>{}(w));
                return _fr.Draw(wCopy,nbinsx,lowx,highx,nbinsy,lowy,highy,nbinsz,lowz,highz,opt);
            }
        }
        operator[](std::get<0>(what).data())->SetBins(nbinsx,lowx,highx);
        operator[](std::get<1>(what).data())->SetBins(nbinsy,lowy,highy);
        operator[](std::get<2>(what).data())->SetBins(nbinsz,lowz,highz);
        std::vector<std::string> _ww; _ww.push_back(std::get<0>(what)); _ww.push_back(std::get<1>(what)); _ww.push_back(std::get<2>(what));
        return Draw(_ww,opt);
    }
    TObject* Draw(const std::vector<std::string>& what, Option_t* opt="");

    TObject* Book(const std::vector<std::string>& what, Option_t* opt="") {
        return nullptr;
    }

    Long64_t Scan(const std::vector<std::string>& what,Option_t* opt="", Long64_t nEntries=5, Long64_t firstEntry=0);


    void SetSubstitution(const TCut& lhs, const TCut& rhs) { fSubstitutions[lhs.GetTitle()] = rhs.GetTitle(); }

    std::shared_ptr<xVar> fVar;

    std::vector<xCut> fCuts;
    std::vector<xFrame> fFrames; // exist to keep the branch servers alive

    std::map<std::string,std::string> fSubstitutions;

    std::function<void(const std::vector<std::string>& vars, Option_t* opt)> fPostDrawAction;

    void SetPostDrawAction(decltype(fPostDrawAction) action) { fPostDrawAction = action; }
    void SetPostDrawAction(std::function<void()> action) { SetPostDrawAction( [=](const std::vector<std::string>&, Option_t*) { action(); }); }

    void Print() const {
        for (auto &n: getNodes()) {
            std::cout << n.weight.GetName() << ":" << std::endl;
            auto defined = n.node.GetDefinedColumnNames();
            for (auto &c: n.node.GetColumnNames()) {
                std::cout << c << " : " << n.node.GetColumnType(c);
                if (std::find(defined.begin(), defined.end(), c) != defined.end()) std::cout << " [DEFINED]";
                std::cout << std::endl;
            }
        }
    }

  private:
    // FrameType determines how this frame, if its included in another frame, is included (which operator was used)
    // Determines how things get drawn (PLUS is default adding, OR = separate primitives, OROR = separate plots
    enum FrameType {
        PLUS = 0,
        AND,
        ANDAND,
        OR,
        OROR
    };
    FrameType fType = PLUS;

};