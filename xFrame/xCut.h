#pragma once

#include "TCut.h"

class xCut : public TCut {
public:
    using TCut::TCut;
    xCut(const TCut& cut) : TCut(cut) { }

    xCut operator!() const {
        xCut out = !TCut(*this);
        std::string n1 = this->GetName();
        if(!n1.empty() && n1!="CUT") out.SetName(TString::Format("!%s",GetName()));
        return out;
    }

    xCut operator&&(const TCut& rhs) const {
        xCut out = TCut(*this) && rhs;
        std::string n1 = this->GetName();
        std::string n2 = rhs.GetName();
        if (n1=="CUT") n1="";
        if (n2=="CUT") n2="";
        if (!n2.empty()) {
            if (!n1.empty()) n1+=",";
            n1 += n2;
        }
        out.SetName(n1.c_str());
        return out;
    }

    xCut operator||(const TCut& rhs) const {
        xCut out = TCut(*this) || rhs;
        std::string n1 = this->GetName();
        std::string n2 = rhs.GetName();
        if (n1=="CUT") n1="";
        if (n2=="CUT") n2="";
        if (!n2.empty()) {
            if (!n1.empty()) n1+="||";
            n1 += n2;
        }
        n1 = "[" + n1 + "]";
        out.SetName(n1.c_str());
        return out;
    }

    xCut operator&&(const char* rhs) const {
        return (*this)&&xCut(rhs);
    }
    xCut operator||(const char* rhs) const {
        return (*this)||xCut(rhs);
    }

    std::string GetComponentFilter() const {
        std::string s(GetTitle());
        if (s.find("_xcomp==")!=0) return "";
        return s.substr(strlen("_xcomp=="));
    }

};
