#pragma once

#include "RooRealVar.h"
#include "TAttMarker.h"
#include "TAttFill.h"
#include "TStyle.h"
#include "TChain.h"
#include "ROOT/RDataFrame.hxx"
#include "TCut.h"

#include "RooUniformBinning.h"

// A Frame holds an xVar - this is used to create dependencies between frames (via dependencies of their xVars)
// But the xVar also holds an object (fObj) which can end up being the data source of this frame
class xVar : public RooRealVar, public TAttMarker, public TAttFill, public TAttLine {
public:
    using RooRealVar::RooRealVar;
    void UseCurrentStyle() override {
        SetFillColor(gStyle->GetHistFillColor());
        SetFillStyle(gStyle->GetHistFillStyle());
        SetLineColor(gStyle->GetHistLineColor());
        SetLineStyle(gStyle->GetHistLineStyle());
        SetLineWidth(gStyle->GetHistLineWidth());
        SetMarkerColor(gStyle->GetMarkerColor());
        SetMarkerStyle(gStyle->GetMarkerStyle());
        SetMarkerSize(gStyle->GetMarkerSize());
    }

    void SetBins(int nbins, double low, double high, const char* binningName=nullptr) {
        setBinning(RooUniformBinning(low,high,nbins),binningName);
    }

    ~xVar() { }

    std::shared_ptr<TObject> fObj; // when this var is really a leaf node, this is the actual arg inside

    struct NamedNode {
        std::string name;
        std::string cuts; // comma separated list of named cuts!
        std::map<std::string,std::string> substitutions;
        TCut totalCut = TCut("",""); // for debugging ... get nodes total cut expression (wont include Filters from modifiers)
        TCut weight = TCut("","");
        std::size_t hash = std::hash<std::string>{}(name);
        ROOT::RDF::RNode node;
        std::shared_ptr<NamedNode> rootNode = nullptr;
        std::weak_ptr<xVar> originator; // weak so that can be put in fSavedNodes without causing a memory leak by owning self
        void addHash(size_t rhs) {
            //hash ^= rhs;
            hash ^= rhs + 0x9e3779b9 + (hash << 6) + (hash >> 2);
        }
    };
    std::vector<std::shared_ptr<NamedNode>> fSavedNodes;

    template <typename T=TObject> T* get() const { return dynamic_cast<T*>(fObj.get()); }

    bool isFundamental() const override { return servers().empty(); }

    bool hasData() const {
        // check if we or any dependents have data source
        if (get<TChain>()) return true;
        for(auto& s : servers()) {
            if (auto v = dynamic_cast<xVar*>(s)) {
                if (v->hasData()) return true;
            }
        }
        return false;
    }

    virtual NamedNode DefineFor(NamedNode node);

    void SetDrawOption	(	Option_t * 	option = ""	) override {
        setStringAttribute("DrawOption",option);
    }
    Option_t* GetDrawOption() const override {
        if(auto&& x = getStringAttribute("DrawOption")) return x;
        return TObject::GetDrawOption();
    }


    std::vector<std::function<ROOT::RDF::RNode(ROOT::RDF::RNode)>> fModifiers;

};