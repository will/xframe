#pragma once

#include "xFrame/xVar.h"
#include "TPRegexp.h"

template<typename T=int,typename W=int> class xMap : public xVar, public std::map<T,W> {
public:
    xMap(const char* name, const char* title) : xVar(name,title,0) {
        fInput = xVar::GetTitle();
    }

    xMap(const char* name, const char* title, const W& defaultVal) : xVar(name,title,0), m_default(defaultVal), fHasDefault(true) {
        fInput = xVar::GetTitle();
    }

    xMap(const xMap& other, const char* name = 0) : xVar(other,name), std::map<T,W>(other), fInput(other.fInput), m_default(other.m_default), fHasDefault(other.fHasDefault)  {
        //std::cout << "cloned " << other.GetName() << " " << this << std::endl;
        //RooArgSet s; for(auto& f : fFrames) s.add(f);redirectServers(s);
    }

    virtual TObject* clone() { return new xMap(*this); }

    //Double_t evaluate() const { return 0; }

    NamedNode DefineFor(NamedNode node) override {
        // check if input is a vector type, it is we will loop over vectors
        // split inputs by ; char
        TStringToken pattern(fInput.c_str(),";");
        std::vector<std::string> inputs;
        while(pattern.NextToken()) inputs.push_back(pattern.Data());

        if (inputs.size()>1) {
            // assume using vector, so need to "pack" the values first
            std::string funcDef = "std::vector<int> out;";
            for(auto& i : inputs) funcDef += TString::Format("out.push_back(%s);",i.c_str());
            funcDef += "return out;";
            node.node = node.node.Define(std::string(xVar::GetName())+"_input",funcDef);
            inputs.clear();
            inputs.push_back(std::string(xVar::GetName())+"_input");
        }

        auto f = [this](T input) {
            if (auto x = this->find(input); x != this->end()) return x->second;
            if (this->fHasDefault) return this->m_default;
            throw std::runtime_error(TString::Format("%s lookup failed and has no default",this->GetName()));
            /*try { return this->at(input); }
            catch (std::exception &) {
                if(this->fHasDefault) return this->m_default;
                throw std::runtime_error(TString::Format("%s lookup failed and has no default",this->GetName()));
            }*/ };
        if(node.node.GetColumnType(inputs.at(0)).find("ROOT::RVec")==0) {
            node.node = node.node.Define(xVar::GetName(), [this,f](ROOT::RVec<T> &input) {
                return ROOT::VecOps::Map(input,f); }, inputs);
            return node;
        } else {
            node.node = node.node.Define(xVar::GetName(), f, inputs);
            return node;
        }
    }

    W m_default;
    bool fHasDefault = false;
    std::string fInput;

};
