
#include "xFrame/xVar.h"

#include <fstream>


xVar::NamedNode xVar::DefineFor(NamedNode node) {

    for(auto& m : fModifiers) {
        node.node = m(node.node);
        node.addHash(std::hash < std::string > {}(GetTitle())); // can we do better?
    }
    if (!fModifiers.empty()) return node;

    if(isFundamental() && !hasData()) {
        if(getStringAttribute("formula")) {
            node.node =  node.node.Define(GetName(),getStringAttribute("formula"));
            node.addHash( std::hash < std::string > {}(getStringAttribute("formula")) );
        } else if(getStringAttribute("filename")) {
            try {
                std::ifstream t(getStringAttribute("filename"));
                std::stringstream buffer;
                buffer << t.rdbuf();
                if (buffer.str().empty()) throw std::runtime_error(std::string("Unable to load ") + getStringAttribute("filename"));
                node.node = node.node.Define(GetName(), buffer.str());
                //std::cout << "Loaded " << getStringAttribute("filename") << " -> " << node.node.GetColumnType(GetName()) << std::endl;
                node.addHash( std::hash < std::string > {}(buffer.str()));
            } catch(...) {
                std::cout << "Failed to load filename column: " << GetName() << " : " << getStringAttribute("filename") << std::endl;
            }
        } else {
            // see if the title can be used as a formula
            try {
                node.node = node.node.Define(GetName(), GetTitle());
                node.addHash( std::hash < std::string > {}(GetTitle()) );
            } catch(...) {
                // if n was already a defined column then don't print warning
                // in 6.26 we can try to do a redefine except on input data columns
                std::cout << "Dont know how to apply " << GetName() << std::endl;
            }
        }
        // TODO: create a template class to hold a generic definition - class is a fundamental
        // with method to "addDefinition(node)" to a node.
    }

    // now loop for fundamentals, adding formula branches and code branches
    for(auto& s : servers()) {
        if (s->getAttribute("isDataColumn")) continue; // these are subframes that only exist to do attributes of columns of data
        if (auto x = dynamic_cast<xVar*>(s)) {
            if (x->hasData()) continue; // skip data servers as already made these subcomponents
            //if (auto xx = std::dynamic_pointer_cast<::xVar>(x->fObj)) {
            //    node = xx->DefineFor(node);
            //}
            node = x->DefineFor(node);
        }
    }



    return node;
}