#include "xFrame/xFrame.h"

#pragma link C++ class xCut+;
#pragma link C++ class xMap<int,int>+;
#pragma link C++ class xFrame+;
#pragma link C++ class xPlot+;

#pragma link C++ class xFrame::HiddenObject+;