//
// Created by Will Buttinger on 2022-02-22.
//

#include "Rtypes.h"
#include "Riostream.h"
#include "TEnv.h"

#include "xFrameVersion.h"


using namespace std;

Int_t doxframeBanner();

static Int_t dummyFMB = doxframeBanner() ;

Int_t doxframeBanner()

{
#ifndef __XFRAME_NOBANNER
    cout << "\033[1mxFrame -- Data Exploration and Visualization -- Development ongoing\033[0m " << endl
         << "                ..." << endl << "                Version: " << GIT_COMMIT_HASH <<
         endl;
#endif
    (void) dummyFMB;
    return 0 ;
}