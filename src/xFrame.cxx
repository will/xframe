#include "xFrame/xFrame.h"

#include "xFrame/xPlot.h"

#include "TSystem.h"
#include <regex>

std::vector<std::tuple<ROOT::RDF::RResultHandle, std::function<void(ROOT::RDF::RResultHandle&)>, std::shared_ptr<xVar::NamedNode>>> xFrame::fBookings;


xFrame::xFrame(const TObject& obj) : fVar(std::make_shared<xVar>(obj.GetName(),obj.GetTitle(),1)) {
    if (obj.IsA() != RooRealVar::Class()) {
        fVar->fObj.reset(obj.Clone(obj.GetName()));
    }
    fVar->UseCurrentStyle();
}

xFrame::xFrame(const TChain& c) : xFrame((const TObject&)c) {
    // Clone of TChain is meaningless ..
    fVar->fObj = std::make_shared<TChain>(c.GetName(),c.GetTitle());
    fVar->get<TChain>()->Add(const_cast<TChain*>(&c));
}

std::vector<std::string> xFrame::GetComponentNames() const {
    // Could improve this in future - not building the nodes to get the names
    // depends if becomes too slow?
    std::vector<std::string> out;
    for(auto& n : getNodes()) {
        out.push_back(n.weight.GetName());
    }
    return out;
}

std::set<std::string> xFrame::GetColumnNames(bool onlyCommon) const {
    std::map<std::string,int> counts;
    int nNodes = 0;
    for(auto& n : getNodes()) {
        nNodes++;
        for(auto& x : n.node.GetColumnNames()) {
            counts[x]++;
        }
    }
    std::set<std::string> out;
    for(auto& [s,c] : counts) if(!onlyCommon || c==nNodes) out.insert(s);
    return out;
}


void decomposeCut(std::string cut, std::vector<std::string>& cuts) {
    //looking for cut to be of form (XXX)&&(YYY) or XXX&&YYY where XXX is 'closed'

    //go through characters looking for closing brace
    TString sCut(cut);
    std::vector<int> bracePos;
    int i=0;
    for(;i<sCut.Length();i++) {
        if(sCut(i)=='(') {bracePos.push_back(i);}
        else if(sCut(i)==')') {
            // if we are popping a first-position brace and we are at end, then decompose inside
            if (bracePos.back()==0 && i==sCut.Length()-1) {
                decomposeCut(cut.substr(1,cut.length()-2),cuts);
                return;
            }
            bracePos.pop_back();
        }
        if(bracePos.size()==0 && sCut(i)=='&' && sCut(i+1)=='&') {
            decomposeCut(cut.substr(0,i),cuts);
            decomposeCut(cut.substr(i+2),cuts);
            return;
        } /*else if(bracePos.empty() && sCut(i)=='*') {
            decomposeCut(cut.substr(0,i),cuts);
            decomposeCut(cut.substr(i+1),cuts);
            return;
        }*/
    }
    // if got here, couldn't decompose, just add the cut unless the cut is nothing
    if (!cut.empty()) cuts.push_back(cut);
}

void xFrame::getNodesInternal(std::map<RooAbsArg*,std::vector<xVar::NamedNode>>& out) const {
    if (out.find(fVar.get())!=out.end()) return; // don't do again

    int nFrames = 0;
    for(auto& f : fFrames) {
        // vertical addition creates new components of the frame
        // so if we have more than 1 dependent vertical frame then we add component name to distinguish
        if (!f->hasData()) continue;
        nFrames++;
    }

    for(auto& f : fFrames) {
        // skip "fundamental" frames
        if (!f->hasData()) continue;
        f.getNodesInternal(out);
        if (out.find(f.fVar.get()) != out.end()) {
            for(auto& n : out[f.fVar.get()]) {
                out[fVar.get()].push_back(n);
                std::string oldName = out[fVar.get()].back().weight.GetName();
                if (nFrames > 1) {
                    out[fVar.get()].back().weight.SetName(TString::Format("%s%s%s",f->GetName(),oldName.empty() ? "" : ".", oldName.c_str()));
                    out[fVar.get()].back().originator = f.fVar;
                }
            }
        }
    }

    // create a RDataFrame if have datasource attribute
    if (auto chain = fVar->get<TChain>()) {
        // decide if we can reuse a saved node
        if (!fVar->fSavedNodes.empty()) {
            if (fVar->getStringAttribute("nFiles") && TString(fVar->getStringAttribute("nFiles")).Atoll()==chain->GetListOfFiles()->GetEntries()) {
                // can reuse - do nothing
            } else {
                fVar->fSavedNodes.clear(); // need to make a new frame;
            }
        }
        if (fVar->fSavedNodes.empty()) {
            // creating new dataframe
            fVar->fSavedNodes.emplace_back(std::make_shared<xVar::NamedNode>(xVar::NamedNode{.name=fVar->GetName(),.node=ROOT::RDataFrame(*chain)}));
        }

        out[fVar.get()].push_back(*fVar->fSavedNodes.at(0));
        out[fVar.get()].back().rootNode = fVar->fSavedNodes.at(0);
        out[fVar.get()].back().originator = fVar;

//            // instead of using an attribute, allow a TChain (or any TObject) to be stored in the
//            // xVar, then if the obj is a TChain then we know what to do
//            TString s(fVar->getStringAttribute("datasource")); // todo: split by ';' char and allow for treeName specify (with filename:treeName?)
//            std::vector<std::string> ss; ss.push_back(s.Data());
//            //std::cout << "buidling dataframe" << std::endl;
//            out[fVar.get()].push_back(std::make_pair(ROOT::RDataFrame(fVar->GetTitle(),ss),TCut("","")));
//            //auto& myCut = out[fVar.get()].back().second;
//            //auto& n = out[fVar.get()].back().first;
//            //n = n.Define("_xcomp",[myCut](){return std::string(myCut.GetName());});
//            // can't do this, even with smart pointers, because need different myCut value in each bridge
    }


    for(auto& n : out[fVar.get()]) {
        n = fVar->DefineFor(n);
        for(auto& sub : fSubstitutions) n.substitutions[sub.first] = sub.second;
    }

//    // now loop for fundamentals, adding formula branches and code branches
//    for(auto& s : fVar->servers()) {
//        if (s->getAttribute("isDataColumn")) continue; // these are subframes that only exist to do attributes of columns of data
//        if (auto x = dynamic_cast<xVar*>(s)) {
//            if (x->hasData()) continue; // skip data servers as already made these subcomponents
//            if (auto xx = std::dynamic_pointer_cast<::xVar>(x->fObj)) {
//                for(auto& n : out[fVar.get()]) {
//                    n = xx->DefineFor(n);
//                }
//                continue;
//            }
//        }
//        if (auto x = dynamic_cast<::xVar*>(s)) {
//            for(auto& n : out[fVar.get()]) {
//                n = x->DefineFor(n);
//            }
//        }
//    }

    // apply cuts
    for(auto& c : fCuts) {
        if(auto s = c.GetComponentFilter(); !s.empty()) {
            // - special cut of _xcomp==regex will filter whole components
            std::vector<xVar::NamedNode> filteredNodes;
            for (auto& n: out[fVar.get()]) {
                if (std::regex_search(n.weight.GetName(),std::regex(s))) filteredNodes.emplace_back(n);
            }
            out[fVar.get()] = filteredNodes;
            continue;
        }
        for (auto& n: out[fVar.get()]) {
            // decomposing the cut can help performance - potentially saves calculating columns needed
            // for later parts of the expression
            std::vector<std::string> cuts;
            decomposeCut(c.GetTitle(),cuts);
            for(auto& cut : cuts) {
                TString s(cut.c_str());
                for(auto& sub : n.substitutions) {
                    if(s==sub.first) {
                        s=sub.second;//std::cout << "Replaced " << cut << " with " << sub.second << " in " << n.weight.GetName() << std::endl;
                    }
                }
                n.node = n.node.Filter(s.Data());
                n.totalCut = n.totalCut&&s;
                n.addHash( std::hash<std::string>{}(s.Data()) );
            }
            n.originator = fVar;
            //n.totalCut = n.totalCut&&c;
            //n.node = n.node.Filter(c.GetTitle());
            if (std::string(c.GetName())!="CUT" && strlen(c.GetName())>0) {
                if(!n.cuts.empty()) n.cuts += ",";
                n.cuts += c.GetName();
            }
            //n.addHash( std::hash<std::string>{}(c.GetTitle()) );
        }
    }

    // apply weights
    for (auto& n: out[fVar.get()]) {
        if (fVar->getStringAttribute("weight")) {
            n.weight *= fVar->getStringAttribute("weight");
            n.originator = fVar;
        }
    }

    // finally apply any modifiers attached to our var
    for(auto& m : fVar->fModifiers) {
        for (auto& n: out[fVar.get()]) {
            n.node = m(n.node);
            n.addHash(std::hash < std::string > {}(fVar->GetTitle())); // can we do better?
        }
    }


}

std::string xFrame::GetCacheFileName() const {

    std::function<std::string(RooAbsArg*)> _recurse;
    _recurse = [&_recurse](RooAbsArg* a) -> std::string {
        if (!a) return "";
        if (a->getStringAttribute("cacheFileName")) { return a->getStringAttribute("cacheFileName"); }
        for(auto& s : a->servers()) {
            auto x = _recurse(s);
            if (!x.empty()) return x;
        }
        return "";
    };
    return _recurse(fVar.get());
}

void xFrame::Process(int progress) {

    // TODO: allow process of a subset (i.e. for a specific subset of root nodes only - saves on running bookings we dont need)

    if (fBookings.empty()) return;

    std::vector<ROOT::RDF::RResultHandle> results;

    std::unordered_map<std::shared_ptr<xVar::NamedNode>,std::tuple<ROOT::RDF::RResultPtr<ULong64_t>,long,TStopwatch>> counters;
    std::unordered_map<std::shared_ptr<xVar::NamedNode>,std::mutex> counter_mutex; // too lazy to figure out how to join these above

    for(auto& b : fBookings) {
        results.push_back(std::get<0>(b));
        auto rn = std::get<2>(b);
        if (rn && counters.find(rn) == counters.end() && progress>0) {
            // create counter for this data source
            counters.emplace(std::pair{rn,std::tuple(rn->node.Count(),0, TStopwatch())});
            counter_mutex.emplace(std::piecewise_construct, std::make_tuple(rn), std::make_tuple());

            auto& mut = counter_mutex[rn];

            int everyN=progress;
            std::string _name = rn->name;

            //TStopwatch s2;
            long& runningTotal = std::get<1>(counters[rn]);
            TStopwatch& timer = std::get<2>(counters[rn]);
            int counterNum = counters.size()-1;
            std::get<0>(counters[rn]).OnPartialResultSlot(everyN,[&mut,_name,&runningTotal,everyN,counterNum,&timer/*,&counts,,totEvents*/](unsigned int slot, ULong64_t& c) {
                std::lock_guard<std::mutex> l(mut);
                timer.Stop();
                runningTotal += everyN;
                std::cout << "\r";
                for(int i =0;i<counterNum;i++) std::cout << "\e[K";
                std::stringstream rate; rate << std::fixed << std::setprecision(3) << std::setfill('0') << std::setw(5) << 1e-6*(runningTotal/timer.RealTime()) << " MHz";
                std::cout << _name << ": " << runningTotal << " Events Processed (" << rate.str() << ")";
                //if(totEvents != TTree::kMaxEntries) std::cout << " " << 100*(double(count)/totEvents) << "% \t (" << everyN/s2.RealTime() << " Hz)";
                std::cout << std::flush;
                timer.Continue();
                //s2.Start();
            });
            results.push_back(std::get<0>(counters[rn]));

        }

    }

    TStopwatch s; s.Start();
    for(auto& [_,t] : counters) std::get<2>(t).Start();
    ROOT::RDF::RunGraphs(results);
    s.Stop();

    bool showTime = false;
    for(auto& c : counters) {
        if(std::get<0>(c.second).GetValue() >= progress) {
            if (!showTime) std::cout << "\r";
            std::cout << c.first->name << ": " << std::get<0>(c.second).GetValue() << " Events Processed"  << std::endl;
            showTime=true;
        }
    }
    if(showTime) {
        std::cout << "Total Processing Time: " << s.RealTime() << " seconds " << std::endl;
    }

    // run the post-processing functions
    for(auto& b : fBookings) {
        std::get<1>(b)(std::get<0>(b));
    }

    fBookings.clear();

}

void xFrame::Book(ROOT::RDF::RResultHandle result, const std::shared_ptr<xVar::NamedNode>& rootNode, const std::function<void(ROOT::RDF::RResultHandle&)>& finalize) {
    fBookings.push_back(std::make_tuple(result,finalize,rootNode));
}


Long64_t xFrame::GetEntries() {
    auto nodes = getNodes();
    std::vector<ROOT::RDF::RResultHandle> counts;
    for(auto& n : nodes) {
        counts.push_back( n.node.Count() );
        Book(counts.back(), n.rootNode);
    }
    if (counts.empty()) return 0;
    //ROOT::RDF::RunGraphs(counts);
    Process();
    Long64_t out = 0;
    for(auto& c : counts) { out += c.GetValue<ULong64_t>(); }
    return out;
}

Double_t xFrame::GetIntegral() {

    // draw a dummy variable ("_getIntegral") with goff, loop over hists in the returned pad and combine integrals
    auto result = Draw("0",1,-1,1,"goff");

    if (!result) return std::numeric_limits<double>::quiet_NaN();

    auto p = dynamic_cast<TVirtualPad*>(result);

    double out = 0;
    for(auto&& o : *p->GetListOfPrimitives()) {
        if(auto h = dynamic_cast<TH1*>(o)) {
            out += h->Integral(0,h->GetNcells());
        }
    }
    delete result;
    return out;
}

Long64_t xFrame::Scan(const std::vector<std::string>& what,Option_t* opt, Long64_t nEntries, Long64_t firstEntry) {
    bool disabled=false;
    if (ROOT::IsImplicitMTEnabled()) {
        disabled = true;
        ROOT::DisableImplicitMT();
    }

    std::vector<ROOT::RDF::RResultHandle> scans;
    std::vector<std::string> scanNames;
    std::map<std::string,std::string> replacements;

    for(auto& n : getNodes()) {
        std::vector<std::string> colNames = {"rdfentry_"};
        auto whatCopy = what;
        if (strcmp(n.weight.GetTitle(),"")!=0) {
            whatCopy.push_back(n.weight.GetTitle());
        }
        for(auto& w : whatCopy) {
            colNames.push_back(w);
            if (!n.node.HasColumn(w)) {
                //colNames.back() = colNames.back() + "_";
                try {
                    //std::cout << "Defining " << w << std::endl;
                    n.node = n.node.Define(w, w);
                } catch(...) {
                    auto ww = colNames.back();
                    colNames.back() = TString::Format("_b%zu", std::hash < std::string > {}(colNames.back())).Data();
                    //std::cout << "Defining " << colNames.back() << " = " << ww << std::endl;
                    if (!n.node.HasColumn(colNames.back())) n.node = n.node.Define(colNames.back(), ww);
                    ww.resize(colNames.back().length(),' ');
                    replacements[colNames.back()] = ww;
                }
            }
        }

        scans.push_back(n.node.Range(firstEntry,(nEntries==-1) ? 0 : (firstEntry+nEntries)).Display(colNames,nEntries));
        scanNames.push_back(n.weight.GetName());

    }

    if (scans.empty()) {if (disabled) ROOT::EnableImplicitMT();return 0;}
    ROOT::RDF::RunGraphs(scans);

    int ii=0;
    for(auto& s : scans) {
        std::cout << scanNames.at(ii) << ":" << std::endl;
        ii++;
        auto _out = s.GetPtr<ROOT::RDF::RDisplay>()->AsString();
        auto _header = _out.substr(0,_out.find('\n'));
        for(auto& x : replacements) {
            auto i = _header.find(x.first);
            if (i == std::string::npos) continue;
            _header.replace(i,x.first.length(),x.second);
        }
        std::cout << _header << std::endl << _out.substr(_out.find('\n')+1);
    }

    if (disabled) ROOT::EnableImplicitMT();
    return 0;
}

TObject* xFrame::Draw(const std::vector<std::string>& what, Option_t* opt) {
    TString sOpt(opt);sOpt.ToLower();
    bool _optPostpone = false;if (sOpt.Contains("postpone")) { _optPostpone = true; sOpt.ReplaceAll("postpone",""); }
    bool _optGoff = false;if (sOpt.Contains("goff")) { _optGoff = true; sOpt.ReplaceAll("goff",""); }
    std::unique_ptr<TH1> hModel;
    TString binningName = fVar->GetName();
    if (what.empty()) return nullptr;

    std::vector<xFrame> vars;
    TString _fullTitle = fVar->GetTitle();
    for(auto& w : what) {
        vars.emplace_back(operator[](w.c_str()));
        if (!vars.back()->hasMax(binningName)) {
            Error("Draw", "Must specify a max value for %s", vars.back()->GetName());
            return nullptr;
        } else if (!vars.back()->hasMin(binningName)) {
            Error("Draw", "Must specify a min value for %s", vars.back()->GetName());
            return nullptr;
        }
        _fullTitle += ";";
        _fullTitle += vars.back()->GetTitle();
        if(strlen(vars.back()->getUnit())) { _fullTitle += TString::Format(" [%s]",vars.back()->getUnit()); }
    }


    if (vars.at(0)->getBinning(binningName).isUniform()) {
        if (what.size()>1) {
            if(what.size()>2) {
                if (vars.at(2)->getBinning(binningName).isUniform()) {
                    if (vars.at(1)->getBinning(binningName).isUniform()) {
                        hModel.reset(new TH3D(fVar->GetName(), _fullTitle, vars.at(0)->numBins(binningName),
                                              vars.at(0)->getMin(binningName), vars.at(0)->getMax(binningName),
                                              vars.at(1)->numBins(binningName), vars.at(1)->getMin(binningName),
                                              vars.at(1)->getMax(binningName),vars.at(2)->numBins(binningName),vars.at(2)->getMin(binningName),vars.at(2)->getMax(binningName)));
                    } else {
                        // not supported by TH3D constructor
                    }
                }
            } else {
                if (vars.at(1)->getBinning(binningName).isUniform()) {
                    hModel.reset(new TH2D(fVar->GetName(), _fullTitle, vars.at(0)->numBins(binningName),
                                          vars.at(0)->getMin(binningName), vars.at(0)->getMax(binningName),
                                          vars.at(1)->numBins(binningName), vars.at(1)->getMin(binningName),
                                          vars.at(1)->getMax(binningName)));
                } else {
                    hModel.reset(new TH2D(fVar->GetName(), _fullTitle, vars.at(0)->numBins(binningName),
                                          vars.at(0)->getMin(binningName), vars.at(0)->getMax(binningName),
                                          vars.at(1)->numBins(binningName),
                                          vars.at(1)->getBinning(binningName).array()));
                }
            }
        } else {
            hModel.reset(new TH1D(fVar->GetName(), _fullTitle, vars.at(0)->numBins(binningName),vars.at(0)->getMin(binningName), vars.at(0)->getMax(binningName)));
        }
    } else {
        if (what.size()>1) {
            if (vars.at(1)->getBinning(binningName).isUniform()) {
                hModel.reset(new TH2D(fVar->GetName(), _fullTitle, vars.at(0)->numBins(binningName),vars.at(0)->getBinning(binningName).array(),vars.at(1)->numBins(binningName),vars.at(1)->getMin(binningName), vars.at(1)->getMax(binningName)));
            } else {
                hModel.reset(new TH2D(fVar->GetName(), _fullTitle, vars.at(0)->numBins(binningName),vars.at(0)->getBinning(binningName).array(), vars.at(1)->numBins(binningName), vars.at(1)->getBinning(binningName).array()));
            }
        } else {
            hModel.reset(new TH1D(fVar->GetName(), _fullTitle, vars.at(0)->numBins(binningName), vars.at(0)->getBinning(binningName).array()));
        }
    }
    if (!hModel) {
        Error("Draw","Unsupported draw");
        return nullptr;
    }

    hModel->SetDirectory(0); hModel->Sumw2();

    std::vector<std::tuple<TH1*,std::set<std::string>,xVar::NamedNode>> draws;
    std::vector<ROOT::RDF::RResultHandle> _draws;

    auto _cacheFile = GetCacheFileName();
    std::unique_ptr<TFile> _cache; if(!_cacheFile.empty() && !gSystem->AccessPathName(_cacheFile.c_str())) { _cache.reset(new TFile(_cacheFile.c_str())); }

    std::string cutString;

    for(auto& n : getNodes()) {
        std::string weightName = n.weight.GetTitle();
        auto _n = n.node;
        if (!weightName.empty() && !_n.HasColumn(weightName)) {
            _n = n.node.Define("_weight",weightName);
            n.addHash( std::hash<std::string>{}(weightName) );
            weightName = "_weight";
        }
        for(auto& w : what) {
            n.addHash(std::hash<std::string>{}(w));
        }

        auto originator = n.originator;// this->operator[](n.weight.GetName()).operator->(); /* TODO: should really add an "originator" to the getNodes return to track the originator of a node*/
        // first see if we already have a draw from this node
        // before booking another fill, see if one of the other components we've already done
        // are for exactly the same thing (happens when 'composing' frames from other frames)
        // in which case we can just 'steal' the histogram (do we need to steal a copy?)
        // the check is by comparing the hash

        bool skip(false);
        for(auto& [a,b,c] : draws) {
            if(c.hash==n.hash) {
                std::cout << " Reusing " << a->GetName() << " for " << n.weight.GetName() << std::endl;
                std::cout << c.totalCut << " vs " << n.totalCut << std::endl;
                std::cout << " cuts == ? " << (std::string(c.totalCut.GetTitle())==std::string(n.totalCut.GetTitle())) << std::endl;
                b.insert(n.weight.GetName());
                // do we need to check the cuts match too??
                // don't need to draw again ... I think code below will just return the same object when traversing
                skip=true;break;
            }
        }
        if(skip) continue;

        if (cutString.empty()) cutString = n.cuts; // should really take the intersection i.e. show cuts common to all nodes ..
        hModel->SetName(n.weight.GetName());
        auto _hist = (TH1*)hModel->Clone(n.weight.GetName()); _hist->SetDirectory(0);
        _hist->SetBit(kInvalidObject); // does this stop root trying to delete it?

        if (!n.cuts.empty()) {
            // perhaps should have a TList of TCuts of all the cuts?
            _hist->GetListOfFunctions()->Add(new TNamed(".namedCuts",n.cuts.c_str())); // owned by list so no leak
        }


        auto _hash = n.hash; //n.hash ^ (std::hash<std::string>{}(x->GetName()));

        // before booking the fill, if we have a cache try to load from that
        if (_cache) {
            TString _key = TString::Format("%lu",_hash);
            TObject* o = nullptr; int cycle = 1;
            TTree* t = nullptr;

            class CCheck : public TH1D {
            public:
                static bool CheckConsistency(TH1* h, TH1* h2) { return TH1::CheckConsistency(h,h2); }
            };

            while (_hist->TestBit(kInvalidObject) && (o = _cache->Get(_key+";"+(cycle++)))) {
                if(o->IsA() == _hist->IsA()) {
                    // if histogram, must be consistent
                    if(auto h = dynamic_cast<TH1*>(_hist); h) {
                        try {
                            CCheck::CheckConsistency(h,dynamic_cast<TH1*>(o));
                            h->Reset("ICES");
                            h->Add(dynamic_cast<TH1*>(o));
                            h->ResetBit(kInvalidObject); // object now valid
                            //Info("Book","Retrieved %s from %s",h->GetName(),_cache->GetName());h->Print();
                            //std::cout << " hash = " << _hash << std::endl;
                        } catch(...) {
                            // not consistent
                        }
                    }
                }
            }
        }

        if (_hist->TestBit(kInvalidObject)) {

            auto _makeAction = [&]() -> ROOT::RDF::RResultHandle {
                if (what.size()==1) {
                    if (weightName.empty()) {
                        return _n.Histo1D(dynamic_cast<TH1D&>(*hModel),what.at(0));
                    } else {
                        return _n.Histo1D(dynamic_cast<TH1D&>(*hModel), what.at(0), weightName);
                    }
                } else if(what.size()==2) {
                    if (weightName.empty()) {
                        return _n.Histo2D(dynamic_cast<TH2D&>(*hModel),what.at(0), what.at(1));
                    } else {
                        return _n.Histo2D(dynamic_cast<TH2D&>(*hModel), what.at(0), what.at(1), weightName);
                    }
                } else if(what.size()==3) {
                    if (weightName.empty()) {
                        return _n.Histo3D(dynamic_cast<TH3D&>(*hModel),what.at(0), what.at(1), what.at(2));
                    } else {
                        return _n.Histo3D(dynamic_cast<TH3D&>(*hModel), what.at(0), what.at(1), what.at(2), weightName);
                    }
                }
                throw std::runtime_error("Not supported");
            };

//            auto r = (weightName.empty()) ? _n.Histo1D(ROOT::RDF::TH1DModel(dynamic_cast<TH1D &>(*hModel)),
//                                                       what.at(0)) : _n.Histo1D(
//                    ROOT::RDF::TH1DModel(dynamic_cast<TH1D &>(*hModel)), what.at(0), weightName);
            auto r = _makeAction();
            _draws.emplace_back(r);
            auto rn = n.rootNode;
            Book(r,rn,[_hist,_cacheFile,_hash](ROOT::RDF::RResultHandle& result) {
                if(_hist->TestBit(TObject::kNotDeleted)) { // attempt to save us from user deleting object before we process
                    switch(_hist->GetDimension()) {
                        case 1:_hist->Add(result.GetPtr<TH1D>());break;
                        case 2:_hist->Add(result.GetPtr<TH2D>());break;
                        case 3:_hist->Add(result.GetPtr<TH3D>());break;
                    }
                    _hist->ResetBit(kInvalidObject); // since its now filled
                    // save to cacheFile if there is one
                    if(!_cacheFile.empty()) {
                        auto f = std::make_unique<TFile>(_cacheFile.c_str(),"UPDATE");
                        f->WriteTObject(_hist,TString::Format("%lu",_hash));
                        //std::cout << "Saved " << _hist->GetName() << " to cache " << _hash << std::endl;
                    }
                    // if the hist has got a stack in its list of functions (in a Hidden object) then mark the
                    // stack modified and remove the stack from the list
                    //std::cout << "Processed " << _hist->GetName() << std::endl;
                    if(auto s = dynamic_cast<HiddenObject*>(_hist->GetListOfFunctions()->FindObject(".stack"))) {
                        //std::cout << "Found hidden stack in " << _hist->GetName() << std::endl;
                        if(auto p =dynamic_cast<THStack*>(s->fObj)) p->Modified();
                        _hist->GetListOfFunctions()->Remove(s);
                        delete s;
                    }
                    if(auto s = dynamic_cast<HiddenObject*>(_hist->GetListOfFunctions()->FindObject(".pad"))) {
                        if(auto p = dynamic_cast<TVirtualPad*>(s->fObj)) p->Modified();
                        _hist->GetListOfFunctions()->Remove(s);
                        delete s;
                    }
                }
            });
        }
        draws.emplace_back(_hist,std::set<std::string>{n.weight.GetName()},xVar::NamedNode(n));
    }

    auto axisHist = (TH1*)hModel->Clone(".axis");
    std::string _title(fVar->GetTitle()); if(_title.find('[')!=std::string::npos) _title = _title.substr(0,_title.find('['));
    axisHist->SetTitle(_title.c_str());axisHist->SetBinContent(2,0.1);
    axisHist->SetDirectory(0);
    axisHist->GetListOfFunctions()->Add(new TExec("update",
                                                  "auto me = (TH1*)gPad->GetPrimitive(\".axis\");/*cout<<\"updating \" << gPad->GetName() << endl;*/\n"
                                                  "me->SetBinContent(1,0.01);me->SetBinContent(2,0.01);"
                                                  "for(auto&& o : *gPad->GetListOfPrimitives()) {\n"
                                                  "  if(auto h = dynamic_cast<TH1*>(o)) {\n"
                                                  "    me->SetBinContent(1,std::max(h->GetMaximum(),me->GetBinContent(1)));\n"
                                                  "    me->SetBinContent(2,std::min(h->GetMinimum(),me->GetBinContent(2)));\n"
                                                  "  }\n"
                                                  "}; gPad->Modified();") );
    axisHist->SetLineWidth(0);
    axisHist->SetBit(kCanDelete);


    // go through the component frames and let them decide what should be drawn, they build with the draw set
    std::function<TObject*(xFrame*, const decltype(draws)&, const std::string&)> myFunc;

    myFunc = [&myFunc,axisHist,&sOpt](xFrame* f, const decltype(draws)& results, const std::string& compName) {

        TObject* out = nullptr;

        // if any of the results originate from this frame then the return object is just the result
        for(auto& r : results) {
            if (std::get<1>(r).count(compName)){// == f->fVar/*->operator->()*/) {
                // TODO: do we need to take a copy if there's more than 1 comp drawing from this?
                out = std::get<0>(r);
                if (auto _n = dynamic_cast<TNamed*>(out)) {
                    _n->SetTitle((*f)->GetTitle());
                }
                break;
            }
        }

        if (!out) {
            // put objects in a list and return the list
            auto l = new TList; l->SetName((*f)->GetName());
            out = l;

            int nComps = 0; int nPads = 0;
            for(auto& ff : f->fFrames) {
                if (ff.fType==OROR) nPads++;
                if (!ff->hasData()) continue;
                nComps++;
            }

            auto _currPad = gPad;
            if (nPads) {
                // need to divide the current pad
                gPad->Divide(nPads+(nPads==f->fFrames.size()?0:1),1); //+1 because objects from self need own pad unless all OROR
            }
            int padCount = (nPads==f->fFrames.size()?0:1);

            std::vector<FrameType> frameTypes;
            for(auto& _f : f->fFrames) {
                std::string compStr = compName;
                std::string cutString = "";
                if(nComps>1) compStr += std::string((compStr.empty()) ? "" : ".") + (_f)->GetName();
                if (_f.fType == OROR) {
                    // switch to pad (incase further Divides in side)
                    padCount++;
                    _currPad->cd(padCount);
                    gPad->SetName(compStr.c_str());
                    gPad->SetTitle(_f->GetTitle());
                    // draw the axisHist
                    auto a = (TH1*)axisHist->Clone(".axis"); a->SetDirectory(0);
                    a->SetTitle(gPad->GetTitle());
                    if(sOpt.Contains("lego")||sOpt.Contains("surf")) a->Draw("LEGO9FUNC");
                    else a->Draw(sOpt + "FUNC");
                }
                auto o = myFunc(&_f,results,compStr);
                if (o) {
                    // pull objects out of lists and delete the list
                    if (auto ll = dynamic_cast<TList*>(o)) {
                        // can't use AddAll as doesn't transfer draw options
                        TObjOptLink *lnk = (TObjOptLink*)ll->FirstLink();
                        TObject *obj = nullptr;
                        while (lnk) {
                            if(_f.fType==OROR) {
                                // draw in the current pad, which we switched onto
                                TString _opt = sOpt + "same";
                                obj = lnk->GetObject();
                                obj->SetBit(kCanDelete);
                                if (obj->TestBit(kInvalidObject)) {
                                    auto hidden = new HiddenObject(".pad", ".pad");
                                    hidden->SetBit(kCanDelete);
                                    hidden->fObj = gPad;
                                    dynamic_cast<TH1 *>(obj)->GetListOfFunctions()->Add(hidden);
                                }
                                if (auto _cuts = obj->FindObject(".namedCuts"); _cuts) {
                                    cutString = _cuts->GetTitle(); // should really take intersect!
                                }
                                obj->Draw(_opt + lnk->GetOption());
                            } else {
                                l->Add(lnk->GetObject(), lnk->GetOption());
                                frameTypes.push_back(_f.fType);
                            }
                            lnk = (TObjOptLink*)lnk->Next();
                        }
                        ll->Clear("nodelete"); delete ll;
                    } else {
                        l->Add(o); // would specify draw opt here
                        frameTypes.push_back(_f.fType);
                    }
                }
                if (!cutString.empty()) xPlot::AddLabel(cutString.c_str());
            }

            if (nPads) {
                gPad->RedrawAxis(); // creates .axis_copy
                if(nPads==f->fFrames.size()) {
                    _currPad->cd();
                } else {
                    _currPad->cd(1); // draw my bits in here
                }
            }




            // ways to combine are:
            //  overlay: components are kept separate [default]
            //  stackType: hidden (stack not visible)

            std::string thisCompName = compName;
            //if(!thisCompName.empty()) thisCompName += ".";thisCompName += (*f)->GetName();

            // if every subobject is a hist and we have more than 1 we can make a stack
            // do this by default (can add DrawOption to skip, I guess)
            if(nComps>1 && l->GetEntries()>0) {
                TString stackType = ((*f)->getStringAttribute("stackType")) ? (*f)->getStringAttribute("stackType") : "hidden";
                int stackDepth = ((*f)->getStringAttribute("stackDepth")) ? TString((*f)->getStringAttribute("stackDepth")).Atoi() : 1;
                THStack* h = new THStack(TString::Format("%s_stack",thisCompName.c_str()),(*f)->GetTitle());
                // can't use AddAll as doesn't transfer draw options
                TObjOptLink *lnk = (TObjOptLink*)l->FirstLink();
                TObject *obj = nullptr;
                std::vector<std::pair<TObject*,std::string>> others;
                std::set<std::string> blockAdd;
                int ii=-1;
                while (lnk) {
                    ii++;
                    if (frameTypes.at(ii) != PLUS) {
                        others.emplace_back(lnk->GetObject(),lnk->GetOption());
                        lnk = (TObjOptLink*)lnk->Next(); continue;
                    }
                    auto _h = dynamic_cast<TH1 *>(lnk->GetObject());
                    if (_h && _h->GetDimension()<=2) { // stacks only support 1d and 2d hists
                        if(!blockAdd.count(_h->GetName())) {
                            h->Add(_h, lnk->GetOption());
                        } else {
                            others.emplace_back(_h,"axis"); // hides it from showing up - should we be deleting? maybe not safe if reused elsewhere
                        }
                    } else {
                        // should we allow hists to be pulled out of a stack before giving up here?
                        // yes we should - although actually we will have created the "total hist" so just ignore this stack
                        if (auto _hs = dynamic_cast<THStack *>(lnk->GetObject())) {
                            //std::cout << (*f)->GetName() << " hiding " << _hs->GetName() << std::endl;
                            // count "." separators in name to determine if should take out or not
                            std::string subcomp(_hs->GetName()); subcomp = subcomp.substr(thisCompName.length()); // should start with . now
                            int thisStackDepth = TString(subcomp.data()).CountChar('.');
                            // should check haven't added the total already (happens if the stack was hidden so can't pull it out anyway)
                            subcomp = _hs->GetName(); subcomp = subcomp.substr(0,subcomp.length()-std::string("_stack").length());
                            if(stackDepth > thisStackDepth && !h->GetHists()->FindObject(subcomp.c_str())) {
                                // can pull out hists and discard the stack (and the total)
                                TObjOptLink *lnk2 = (TObjOptLink*)_hs->GetHists()->FirstLink();
                                TObject *obj2 = nullptr;
                                while (lnk2) {
                                  h->Add((TH1*)lnk2->GetObject(), lnk2->GetOption());
                                  lnk2 = (TObjOptLink*)lnk2->Next();
                                }
                                blockAdd.insert(subcomp);
                                others.emplace_back(_hs,std::string(lnk->GetOption()) + "axis"); // could perhaps delete the stack instead of drawing hidden? - but hiddenObject link below
                            } else {
                              others.emplace_back(_hs,std::string(lnk->GetOption()) + ((stackType=="hidden") ? "axis" : "")); // adding axis option will hide the stack
                            }
                        } else {
                            delete h;
                            h = nullptr;
                            break;
                        }
                    }
                    lnk = (TObjOptLink*)lnk->Next();
                }
                if (h && (!h->GetHists() || h->GetHists()->GetEntries()<2)) { // check it has more than 1 hist
                    // could be here because stack is declared hidden ... so we want to reverse the draw order
                    if(stackType=="hidden") {
                        l->Clear("nodelete");
                        if(h->GetHists()) { // the things we could have stacked
                            TObjOptLink *lnk = (TObjOptLink *) h->GetHists()->FirstLink();
                            while (lnk) {
                                // TODO: check if this copies the update exec correctly -- does the total always correctly update?
                                l->Add(lnk->GetObject(),
                                       strlen((*f)->GetDrawOption()) ? (*f)->GetDrawOption() : lnk->GetOption());
                                ((TH1 *) lnk->GetObject())->SetTitle((*f)->GetTitle());
                                *dynamic_cast<TAttMarker *>(lnk->GetObject()) = (*f->fVar.get());
                                *dynamic_cast<TAttLine *>(lnk->GetObject()) = (*f->fVar.get());
                                *dynamic_cast<TAttFill *>(lnk->GetObject()) = (*f->fVar.get());
                                lnk = (TObjOptLink *) lnk->Next();
                            }
                        }
                        for(auto& [a,b] : others) {
                            l->Add(a,b.c_str());
                        }
                    }
                    delete h;h = nullptr;
                }
                if (h) {
                    // add the stack as a hidden object in the functions list of the hist (so that it gets updated when drawn)
                    // only needed on invalid objects
                    for(auto&& o : *l) {
                        if(!dynamic_cast<TH1*>(o)) continue; // skip non hists
                        if (o->TestBit(kInvalidObject)) {
                            auto hidden = new HiddenObject(".stack",".stack"); hidden->fObj = h;
                            dynamic_cast<TH1*>(o)->GetListOfFunctions()->Add(hidden);
                        }
                    }

                    // ok stack is good, remove all the hists and add our stack instead
                    l->Clear("nodelete");
                    // restore ... others
                    // blocked hists should have picked up an "axis" option that will hide them
                    for(auto& [a,b] : others) {
                        l->Add(a,b.c_str());
                    }
                    // overlay a total hist with our current draw style
                    auto htotal = (TH1*)h->GetStack()->Last()->Clone(thisCompName.c_str());htotal->SetDirectory(0);
                    if (stackType!="hidden") {
                        // not hiding so add the stack first and then the total (so it overlays)
                        l->Add(h);l->Add(htotal,(*f)->GetDrawOption() ); // same option added automatically below
                    } else {
                        std::cout << (*f)->GetName() << " hiding " << h->GetName() << std::endl;
                        l->Add(htotal,(*f)->GetDrawOption());l->Add(h,"axis" );
                    }

                    // use an exec to ensure updated automatically
                    h->SetTitle(TString::Format("%s;%s;%s",(*f)->GetTitle(),htotal->GetXaxis()->GetTitle(),htotal->GetYaxis()->GetTitle()));
                    htotal->SetTitle((*f)->GetTitle());
                    htotal->GetListOfFunctions()->Add(new TExec(".update",TString::Format("if(gPad->IsModified()) {if(auto _h = (TH1*)gPad->GetPrimitive(\"%s\")){ _h->Reset(\"ICES\"); if(auto sta = dynamic_cast<THStack*>(gPad->GetPrimitive(\"%s\"))) { bool anyInvalid=false; for(auto&& h : *sta->GetHists()) {_h->Add((TH1*)h);anyInvalid|=h->TestBit(kInvalidObject);}if(!anyInvalid) _h->ResetBit(kInvalidObject); gPad->Modified();}}}", htotal->GetName(),h->GetName())));
                    *dynamic_cast<TAttMarker*>(htotal) = (*f->fVar.get());
                    *dynamic_cast<TAttLine*>(htotal) = (*f->fVar.get());
                    *dynamic_cast<TAttFill*>(htotal) = (*f->fVar.get());
                }
            }
        }

        // stylize
        if (auto o = dynamic_cast<TAttMarker*>(out)) *o = (*f->fVar.get());
        if (auto o = dynamic_cast<TAttLine*>(out)) *o = (*f->fVar.get());
        if (auto o = dynamic_cast<TAttFill*>(out)) *o = (*f->fVar.get());

        // if not a list, put in a list with the draw option
        if (!dynamic_cast<TList*>(out)) {
            auto l = new TList; l->SetName((*f)->GetTitle());
            l->Add(out,(*f)->GetDrawOption());
            out = l;
        }

        //if(out) out->Print();

        return out;
    };

    // TODO: create an axis histogram with our title, and an exec to set its first two bins to max and min values (With errors)
    // of whatever is in the gPad
    // so that range is auto correct.

    auto tmpPad = gPad;
    TVirtualPad* out = gPad;
    if(_optGoff || !out) {
        auto _tmp = gROOT->IsBatch();
        if(_optGoff) gROOT->SetBatch();
        out = new TCanvas(fVar->GetName(),fVar->GetTitle());
        gROOT->SetBatch(_tmp);
    }

    if(!sOpt.Contains("same")) out->Clear();


    auto _o = myFunc(this, draws, "");
    if (_o) {
        if (auto l = dynamic_cast<TList *>(_o)) {
            if (l->GetEntries()) axisHist->Draw(sOpt + "FUNC"); // get empty list if we are a set of subplots
            // draw each object from the list, with it's draw option, + same
            TObjOptLink *lnk = (TObjOptLink *) l->FirstLink();
            TObject *obj = nullptr;

            while (lnk) {
                TString _opt = sOpt + "same";//(obj) ? "same " : "";
                obj = lnk->GetObject();
                obj->SetBit(kCanDelete);
                if (obj->TestBit(kInvalidObject)) {
                    auto hidden = new HiddenObject(".pad", ".pad");
                    hidden->SetBit(kCanDelete);
                    hidden->fObj = gPad;
                    dynamic_cast<TH1 *>(obj)->GetListOfFunctions()->Add(hidden);
                } else if(auto h = dynamic_cast<TH1*>(obj)) {
                    axisHist->SetBinContent(1,std::max(h->GetMaximum(),axisHist->GetBinContent(1)));
                    axisHist->SetBinContent(2,std::min(h->GetMinimum(),axisHist->GetBinContent(1)));
                }
                obj->Draw(_opt + lnk->GetOption());
                lnk = (TObjOptLink *) lnk->Next();
            }
            l->Clear("nodelete");
            delete l;
        } else {
            _o->SetBit(kCanDelete);
            _o->Draw(sOpt);
        }
    }
    if (!cutString.empty()) xPlot::AddLabel(cutString.c_str());
    gPad->RedrawAxis(); // creates .axis_copy
    if (fPostDrawAction) fPostDrawAction(what, opt);

    if (!_optPostpone) {
        if (!_draws.empty()) Process();

        //        ROOT::RDF::RunGraphs(_draws);
        //        // loop through draws and transfer results
        //        for(auto& [a,b,c] : draws) {
        //            a->Add(b.GetPtr<TH1D>());
        //        }
        if (gPad) {
            gPad->Modified();
            gPad->Update();
        }
        if(!_optGoff) {
            gSystem->ProcessEvents();
            if (gPad) gPad->Modified();
        }
    }
    out->cd(); // switch back to the output pad
    if(tmpPad) gPad = tmpPad; // result original pad
    return out;
}