
#include "xFrame/xPlot.h"
#include <iostream>
#include "TExec.h"
#include "TLegendEntry.h"

TLegend* xPlot::GetLegend(const xPlotOpts& opts) {
    xPlotOpts _opts = opts;
    if (!_opts.pad) _opts.pad = gPad;
    if (!_opts.pad) return nullptr;

    auto l = dynamic_cast<TLegend*>(_opts.pad->GetPrimitive("legend"));
    if (!l && _opts.build) {
        l = new TLegend(0.5,0.6,1.-_opts.pad->GetRightMargin(),1.-_opts.pad->GetTopMargin()); l->SetName("legend");
        l->SetTextSizePixels(10);
        l->SetNColumns(2);
        l->SetHeader("#font[72]{ATLAS} Internal | #sqrt{s} = 13 TeV, 139 fb^{-1}","c");
        ((TLegendEntry*)(l->GetListOfPrimitives()->At(0)))->SetTextSizePixels(12);
        //l->AddEntry((TObject*)nullptr,"#font[72]{ATLAS} Internal","");
        //l->AddEntry((TObject*)nullptr,"#sqrt{s} = 13 TeV, 139 fb^{-1}","");
        TObjOptLink *lnk = (TObjOptLink*)_opts.pad->GetListOfPrimitives()->FirstLink();
        TObject *obj = nullptr;
        while (lnk) {
            TString sOpt (lnk->GetOption()); sOpt.ToLower();
            auto o = lnk->GetObject();
            if (sOpt.Contains("axis") || TString(o->GetName()).BeginsWith('.')) { lnk = (TObjOptLink*)lnk->Next(); continue; } // skips things drawn as "axis"

            if (auto s = dynamic_cast<THStack*>(o)) {
                for(auto&& h : *s->GetHists()) {
                    TH1* hh = (TH1*)h;
                    TString sTitle = opts.format;
                    sTitle.ReplaceAll("%title%",h->GetTitle());
                    double inte,err; inte = hh->IntegralAndError(0,hh->GetNbinsX()+1,err);
                    int expo = (!err) ? -2 : std::log10(err)-2; //up to the dp or how many after
                    if(expo>0) expo=0; else expo = -expo;
                    sTitle.ReplaceAll("%int%",TString::Format(TString::Format("%%.%df",expo),inte));
                    sTitle.ReplaceAll("%interr%",TString::Format(TString::Format("%%.%df",expo),err));
                    //sTitle.ReplaceAll("%int%",TString::Format("%g",inte)); sTitle.ReplaceAll("%interr%",TString::Format("%g",err));
                    l->AddEntry(h,sTitle,"f")->SetTextColor(h->TestBit(kInvalidObject)?kRed:kBlack);
                }
            } else if (o->InheritsFrom("TH1")) {
                TH1* hh = (TH1*)o;
                TString sTitle = opts.format;
                sTitle.ReplaceAll("%title%",o->GetTitle());
                double inte,err; inte = hh->IntegralAndError(0,hh->GetNbinsX()+1,err);
                int expo = (!err) ? -2 : std::log10(err)-2; //up to the dp or how many after
                if(expo>0) expo=0; else expo = -expo;
                sTitle.ReplaceAll("%int%",TString::Format(TString::Format("%%.%df",expo),inte));
                sTitle.ReplaceAll("%interr%",TString::Format(TString::Format("%%.%df",expo),err));
                //sTitle.ReplaceAll("%int%",TString::Format("%g",inte)); sTitle.ReplaceAll("%interr%",TString::Format("%g",err));
                l->AddEntry(o,sTitle)->SetTextColor(o->TestBit(kInvalidObject)?kRed:kBlack);;
            }
            lnk = (TObjOptLink*)lnk->Next();
        }

        l->SetBit(kCanDelete);
        auto _tmp = gPad;
        _opts.pad->cd();
        l->Draw();
        // create auto-updating exec
        // TODO: exec needs handling of THStack
        auto e= new TExec(".update_legend",TString::Format("if(gPad->IsModified()) {\n"
                                          "            if (auto me = (TLegend*)gPad->GetPrimitive(\"%s\")) {\n"
                                          "                bool modified = false;\n"
                                          "                for(auto&& o : *me->GetListOfPrimitives()) {\n"
                                          "                    auto e = dynamic_cast<TLegendEntry*>(o);\n"
                                          "                    if(auto hh = dynamic_cast<TH1*>(e->GetObject())) {\n"
                                          "                        TString sTitle = e->GetObject()->GetTitle();\n"
                                          "                        double inte,err; inte = hh->IntegralAndError(0,hh->GetNbinsX()+1,err);\n"
                                          "                        int expo = (!err) ? -2 : std::log10(err)-2; //up to the dp or how many after\n"
                                                          "        if(expo>0) expo=0; else expo = -expo;\n"
                                                          "        sTitle.ReplaceAll(\"%%int%%\",TString::Format(TString::Format(\"%%%%.%%df\",expo),inte));\n"
                                                          "        sTitle.ReplaceAll(\"%%interr%%\",TString::Format(TString::Format(\"%%%%.%%df\",expo),err));\n"
                                          "                        e->SetLabel(sTitle);e->SetTextColor(hh->TestBit(kInvalidObject)?kRed:kBlack);\n"
                                          "                        modified = true;\n"
                                          "                    }\n"
                                          "                }\n"
                                          "                if (modified) gPad->Modified();\n"
                                          "            }\n"
                                          "        }",l->GetName()));
        e->Draw();
        _tmp->cd();
    }
    return l;
}

TVirtualPad* xPlot::AddAuxPad(const char* name, const char* title, double height, TVirtualPad* pad) {
    if (!pad) pad = gPad;
    if (!pad) return nullptr;

    auto axis = xPlot::GetPrimitive<TH1>(".axis",pad);
    if (!axis) return nullptr; // could not find axis

    double oldMargin = pad->GetBottomMargin();

    // count the number of subpads in the pad .. for now assume these are all aux pads
    int padCount=0;
    for(auto&& o : *pad->GetListOfPrimitives()) {
        if(auto p = dynamic_cast<TVirtualPad*>(o);p) { padCount++; oldMargin += p->GetHNDC(); }
    }

    double oldHeight = pad->GetCanvas()->GetWh()*pad->GetHNDC()*(1.-pad->GetBottomMargin()); // height excluding bottom margin
    double newHeight = oldHeight*(1. + height);

    double newMargin = height/(1.+height);

    pad->GetCanvas()->SetWindowSize(pad->GetCanvas()->GetWindowWidth(),pad->GetCanvas()->GetWindowHeight() - pad->GetCanvas()->GetWh() + newHeight);
    pad->SetBottomMargin(newMargin);

    auto auxPad = new TPad(name,title,0,0,1,newMargin-0.005);
    auxPad->SetNumber(padCount+1);
    auxPad->SetBottomMargin(auxPad->GetBottomMargin()*(1.-newMargin)/newMargin);
    auxPad->SetTopMargin(0.04);
    auxPad->SetLeftMargin(pad->GetLeftMargin());auxPad->SetRightMargin(pad->GetRightMargin());

    auto axisHist = (TH1*)axis->Clone(".axis");
    axisHist->SetBit(TH1::kNoTitle); //axisHist->SetTitle(""); // so no title shown
    axisHist->GetYaxis()->SetTitle(title);
    axisHist->SetDirectory(0);

    axisHist->GetYaxis()->SetNdivisions(5,0,0);
    axisHist->GetXaxis()->SetTitleOffset( axisHist->GetXaxis()->GetTitleOffset() / auxPad->GetHNDC());
    axisHist->GetXaxis()->SetTickLength(axisHist->GetXaxis()->GetTickLength() * (1. - (auxPad->GetHNDC()))/(auxPad->GetHNDC()));
    axisHist->SetMaximum(-1111); axisHist->SetMinimum(-1111);

    auto _tmp = gPad;
    auxPad->cd();
    axisHist->Draw();
    if(_tmp) _tmp->cd();

    auxPad->Draw();

    return auxPad;

}


bool xPlot::AddRatio(const char* denominator, double height, TVirtualPad* pad) {
    if (!pad) pad = gPad;
    if (!pad) return false;

    auto total_ratio = dynamic_cast<TH1*>(pad->GetPrimitive(denominator));
    if (!total_ratio) return false;

    // height needs to be the new bottom margin height

    auto _tmp = gPad;
    pad->cd(); // for GetDrawOption to work

    double oldHeight = pad->GetCanvas()->GetWh()*pad->GetHNDC()*(1.-pad->GetBottomMargin()); // height excluding bottom margin
    double newHeight = oldHeight*(1. + height);
    double oldMargin = oldHeight*pad->GetBottomMargin();
    double newMargin = height/(1.+height);

    pad->GetCanvas()->SetWindowSize(pad->GetCanvas()->GetWindowWidth(),pad->GetCanvas()->GetWindowHeight() - pad->GetCanvas()->GetWh() + newHeight);
    pad->SetBottomMargin(newMargin);


    auto safeDiv = [](double a, double b) { if(b==0 && a==0) return 0.; else if(a==0) return 0.; else if(b==0) return 0.; else return a/b; };

    std::vector<std::pair<TH1*,std::string>> hists;
    TH1* final_ratio = nullptr;
    for(auto&& o : *pad->GetListOfPrimitives()) {
        TString drawOpt = o->GetDrawOption(); drawOpt.ToLower();
        if(TString(o->GetName()).BeginsWith('.') || drawOpt.Contains("axis")) continue;

        auto h = dynamic_cast<TH1*>(o);
        if (!h) continue;
        auto h_ratio = (TH1*)h->Clone(h->GetName());h_ratio->SetDirectory(0);
        h_ratio->SetBit(kCanDelete);
        h_ratio->GetListOfFunctions()->Clear(); // to remove any TExec ... could be more selective




        h_ratio->GetListOfFunctions()->Add(new TExec(".update",TString::Format("if(gPad->GetMother()->IsModified()) {\n"
                                                                              "            auto me = (TH1*)gPad->GetPrimitive(\"%s\");\n"
                                                                              "            if(me) {\n"
                                                                              "                auto numer = (TH1*)gPad->GetMother()->GetPrimitive(me->GetName());\n"
                                                                              "                auto denom = (TH1*)gPad->GetMother()->GetPrimitive(\"%s\");\n"
                                                                              "                if(numer && denom) {\n"
                                                                              "                    auto safeDiv = [](double a, double b) { if(b==0 && a==0) return 0.; else if(a==0) return 0.; else if(b==0) return 0.; else return a/b; };\n"
                                                                              "                    for (int i = 0; i <= me->GetNbinsX()+1; i++) {\n"
                                                                              "                        me->SetBinError(i, safeDiv(numer->GetBinError(i) , denom->GetBinContent(i)));\n"
                                                                              "                        me->SetBinContent(i,safeDiv(numer->GetBinContent(i) , denom->GetBinContent(i)));\n"
                                                                              "                    }\n"
                                                                              "                    gPad->Modified();\n"
                                                                              "                }\n"
                                                                              "            }\n"
                                                                              "        }", h->GetName(), denominator)));

        for (int i = 0; i <= total_ratio->GetNbinsX()+1; i++) {
            h_ratio->SetBinError(i, safeDiv(h_ratio->GetBinError(i) , total_ratio->GetBinContent(i)));
            h_ratio->SetBinContent(i,safeDiv(h_ratio->GetBinContent(i) , total_ratio->GetBinContent(i)));
        }
        if (h==total_ratio) {final_ratio = h_ratio;}
        else hists.push_back({h_ratio,h->GetDrawOption()});
    }

    auto ratioPad = new TPad("ratio","aux plot",0,0,1,newMargin-0.005);
    ratioPad->SetNumber(1);
    ratioPad->SetBottomMargin(ratioPad->GetBottomMargin()*(1.-newMargin)/newMargin);
    ratioPad->SetTopMargin(0.04);
    ratioPad->SetLeftMargin(pad->GetLeftMargin());ratioPad->SetRightMargin(pad->GetRightMargin());
    ratioPad->cd();

    ratioPad->SetGridy(1);

    final_ratio->GetYaxis()->SetNdivisions(5,0,0);
    final_ratio->SetTitle("");
    final_ratio->GetYaxis()->SetTitle("Ratio");
    final_ratio->GetXaxis()->SetTitleOffset( final_ratio->GetXaxis()->GetTitleOffset() / ratioPad->GetHNDC());
    final_ratio->GetXaxis()->SetTickLength(final_ratio->GetXaxis()->GetTickLength() * (1. - (ratioPad->GetHNDC()))/(ratioPad->GetHNDC()));
    final_ratio->SetMaximum(-1111); final_ratio->SetMinimum(-1111);
    final_ratio->Draw("e2");

    for(auto& h : hists) {
        h.first->Draw((h.second+"same").c_str());
        if(h.second.find("hist")!=std::string::npos || h.second.find("HIST") != std::string::npos) {
            // need to draw the update functions on top because in HIST mode the functions aren't drawn
            h.first->GetListOfFunctions()->Draw("same");
        }
    }

    pad->cd();
    ratioPad->Draw();
    pad->Modified();pad->Update();
    _tmp->cd();
    return true;

}

void xPlot::Update(TVirtualPad* pad) {
    if (!pad) {
        pad = gPad;
        if (!pad) return;
        pad = gPad->GetCanvas(); // actually start from the canvas
    }
    // mark all subpads modified
    for(auto&& o : *pad->GetListOfPrimitives()) {
        if(auto p = dynamic_cast<TVirtualPad*>(o)) {
            Update(p);
        }
    }
    pad->Modified();
    if(pad == pad->GetCanvas()) pad->Update();
}

void xPlot::Print(TObject* parent,bool showHidden, int indent) {
    if (!parent) {
        parent = gPad;
        if (!parent) return;
        parent = gPad->GetCanvas(); // actually start from the canvas
    }

    if (strcmp(parent->ClassName(),"TFrame")==0) return; // skip TFrames, boring

    TString drawOpt = parent->GetDrawOption(); drawOpt.ToLower();

    if(!showHidden && (TString(parent->GetName()).BeginsWith('.') || drawOpt.Contains("axis"))) return;

    std::cout << std::string( indent, ' ' ) << " [" << parent->GetDrawOption()<<"]" << parent->ClassName() << "::" << parent->GetName();

    if(auto p = dynamic_cast<TVirtualPad*>(parent)) {
        std::cout << std::endl;
        auto _tmp = gPad;
        p->cd(); // so GetDrawOption works in children
        for(auto&& o : *p->GetListOfPrimitives()) {
            Print(o,showHidden,indent+2);
        }
        _tmp->cd();
    } else if(auto s = dynamic_cast<THStack*>(parent)) {
        std::cout << std::endl;
        for(auto&& o : *s->GetHists()) {
            Print(o,showHidden,indent+1);
        }
    } else if(auto h = dynamic_cast<TH1*>(parent)) {
        double inte,interr; inte = h->IntegralAndError(0,h->GetNbinsX()*h->GetNbinsY()*h->GetNbinsZ(),interr);
        std::cout << ", Entries = " << h->GetEntries() << " Integral = " << inte << " +/- " << interr << std::endl;
        for(auto&& o : *h->GetListOfFunctions()) {
            Print(o,showHidden,indent+1);
        }
    } else if(auto g = dynamic_cast<TGraph*>(parent)) {
        std::cout << ", Points = " << g->GetN() << std::endl;
        for(auto&& o : *g->GetListOfFunctions()) {
            Print(o,showHidden,indent+1);
        }
    } else {
        std::cout << std::endl;
    }

}